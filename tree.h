
//          Copyright Morel Berenger 2012 - 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef TREE_H
#define TREE_H

//#include <utility>
//#include <typeinfo>
#include <vector>
#include <memory>
#include <stack>

template <typename TLeaf>
class Leaf
{
	TLeaf m_leaf;
public:
	Leaf(void)=default;
	Leaf(Leaf<TLeaf> const&)=default;
	Leaf(TLeaf const& leaf)
	:m_leaf(leaf)
	{
	}

	Leaf(TLeaf && leaf)
	:m_leaf(std::move(leaf))
	{
	}

	TLeaf& get(void)
	{
		return m_leaf;
	}

	virtual ~Leaf(void){}
};

template <typename TLeaf>
class Node: public Leaf<TLeaf>
{
	typedef Leaf<TLeaf> Target;
	typedef std::vector<std::unique_ptr<Target>> List;
	List m_children;

public:
	class iterator
	{
		//first data is the level, second one is the offset in the level.
		typedef std::pair<Node<TLeaf>*, typename List::iterator> Position;

		Position m_actual;
		std::stack<Position> m_history;

	public:
		iterator(Node<TLeaf> * level, typename List::iterator const& offset)
		:m_actual(Position(level,offset)), m_history()
		{
		}

		Leaf<TLeaf>& operator*(void)
		{
			return **m_actual.second;
		}

		Leaf<TLeaf>* operator->(void)
		{
			return m_actual.second->get();
		}

		bool operator==(iterator const& other)
		{
			return other.m_actual==m_actual && other.m_history==m_history;
		}
		bool operator!=(iterator const& other)
		{
			return !operator==(other);
		}

		iterator& operator++(void)
		{
			if(m_actual.first->m_children.end()!=m_actual.second)
				++m_actual.second;
			return *this;
		}

		iterator& operator--(void)
		{
			if(m_actual.first->m_children.begin()!=m_actual.second)
				--m_actual.second;
			return *this;
		}

		bool isNode(void)const
		{
			return !reachedEndLevel() && dynamic_cast<Node<TLeaf>*>(m_actual.second->get())!=nullptr;
		}

		bool reachedEndLevel(void)const
		{
			return m_actual.first->m_children.end()==m_actual.second;
		}
	};

	Node(void)=default;

	Node(TLeaf const& other) //initialize
	:Leaf<TLeaf>(other), m_children()
	{
	}

	Node(Node const& other) //copy
	:Leaf<TLeaf>(other), m_children()
	{
		m_children.reserve(other.m_children.size());
		for(auto &i:other.m_children)
		{
			//detect i's type to clone it's type correctly
			if(typeid(*i)==typeid(Node<TLeaf>))//node
				m_children.push_back(std::unique_ptr<Node<TLeaf>>(new Node<TLeaf>(*static_cast<Node<TLeaf>*>(i.get()))));
			else
				m_children.push_back(std::unique_ptr<Leaf<TLeaf>>(new Leaf<TLeaf>(*static_cast<Leaf<TLeaf>*>(i.get()))));
		}
	}

	template <typename T>
	void push_back(T const& t)
	{
		m_children.push_back(std::unique_ptr<T>(new T(t)));
	}


	iterator begin(void)
	{
		return iterator(this,m_children.begin());
	}

	iterator end(void)
	{
		return iterator(this,m_children.end());
	}

	decltype(m_children.size()) levelSize(void)const
	{
		return m_children.size();
	}
};

#endif
