#ifndef FILE_H
#define FILE_H

#include <string>
#include <cstdio>

class File
{
	std::string m_name;
public:
	File(std::string const& name)
	:m_name(name)
	{
//		printf("New Ctor %s\n",m_name.c_str());
	}

	File(File && other)
	:m_name(std::move(other.m_name))
	{
//		printf("Move\n");
	}
	File(File const& other)
	:m_name(other.m_name)
	{
//		printf("Copy Ctor %s\n",m_name.c_str());
	}

	~File()
	{
//		printf("Dtor %s\n",m_name.c_str());
	}

	void p(void)
	{
		printf("%s\n",m_name.c_str());
	}
};

#endif
