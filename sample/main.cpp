#include "file.h"
#include "../tree.h"

typedef Node<File> _Folder;
typedef Leaf<File> _File;

void print(_Folder & origin,std::string prefix);
void rprint(_Folder & origin,std::string prefix);

int main(int argc, char** argv)
{
	_Folder f3(File("sub-folder 1"));
	f3.push_back(_File(File("yet 1")));
	f3.push_back(_File(File("yet 2")));
	f3.push_back(_File(File("yet 3")));
	_Folder f4(File("sub-folder 2"));
	f4.push_back(_File(File("yet 4")));
	f4.push_back(_File(File("yet 5")));
	f4.push_back(_File(File("yet 6")));

	_Folder f2(File("folder 1"));
	f2.push_back(_File(File("another 1")));
	f2.push_back(_File(File("another 2")));
	f2.push_back(f3);
	f2.push_back(f4);

	_Folder *folder=new _Folder(File("root"));
	_Folder &f(*folder);
	f.push_back(_File(File("file 1")));
	f.push_back(_File(File("file 2")));
	f.push_back(_Folder(File("empty folder 1")));

	f.push_back(f2);

	puts("\nnormal print:");
	print(f,"");

	puts("\nreverse print:");
	rprint(f,"");

	delete folder;
}

void print(_Folder & origin,std::string prefix)
{
	printf(prefix.c_str());
	origin.get().p();
	prefix+="\t";
	for(_Folder::iterator it=origin.begin();it!=origin.end();++it)
	{
		if(it.isNode())
			print(dynamic_cast<_Folder&>(*it),prefix);
		else
		{
			printf(prefix.c_str());
			it->get().p();
		}
	}
}

void rprint(_Folder & origin,std::string prefix)
{
	printf(prefix.c_str());
	origin.get().p();
	prefix+="\t";

	if(0==origin.levelSize())
		return;
	_Folder::iterator it=origin.end();
	do
	{
		--it;

		if(it.isNode())
			rprint(dynamic_cast<_Folder&>(*it),prefix);
		else
		{
			printf(prefix.c_str());
			it->get().p();
		}
	}while(origin.begin()!=it);
}
