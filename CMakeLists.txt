cmake_minimum_required( VERSION 2.8 )
enable_language( CXX )

project( tree )

install( FILES "tree.h" DESTINATION "include" )
